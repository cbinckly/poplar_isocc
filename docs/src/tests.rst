Testing
==============

This package includes both Python and Extender Unit testing
as well as an Extender Acceptance Test.

.. autoclass:: poplar_isocc.tests.test_poplar_isocc.IsoCountryCodeTestCase
    :members:
    :show-inheritance:

.. autoclass:: poplar_isocc.tests.extest_poplar_isocc.IsoCountryCodeTestCase
    :members:
    :show-inheritance:
    
.. autoclass:: poplar_isocc.tests.extest_poplar_isocc.IsoCountryCodeAcceptanceTestCase
    :members:
    :show-inheritance:
