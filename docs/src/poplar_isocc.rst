poplar_isocc
==========================================================

This package implements a method to validate a string against
a reference list of country codes.

.. automodule:: poplar_isocc
    :members:
    :show-inheritance:

