=======================
poplar_isocc
=======================

----------------------------
ISO Country Code Enforcement
----------------------------

This package contains an Extender module and accompanying
Python code that can be used to enforce the use of an 
ISO-3166-1 ISO code in any text field in Sage.

More generally, this package demonstrates how to
build Python packages for use with the Python Package
Manager for Orchid Extender.
