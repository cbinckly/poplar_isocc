"""Python unit tests for the poplar_isocc module."""
import unittest
from poplar_isocc import is_valid_iso_cc

class IsoCountryCodeTestCase(unittest.TestCase):
    """Unit tests for ISO country code validation."""

    def test_is_valid_iso_cc(self):
        """Test that the ISO code validation function works as expected.

        1. For each code in a list of valid codes, verify that the validation
           passes.
        2. For each code in a list of invalid codes, including overlength and
           empty values, verify that the validation fails.

        """
        valid_iso_ccs = ['CA', 'AU', 'FR', 'US', ]
        invalid_iso_ccs = ['ZZ', 'YY', "", None, -1, 'USA', "Country", ]

        for cc in valid_iso_ccs:
            self.assertTrue(is_valid_iso_cc(cc))

        for cc in invalid_iso_ccs:
            self.assertTrue(not is_valid_iso_cc(cc))

