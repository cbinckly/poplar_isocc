"""Extender unit and acceptance tests for the POISOCC module."""
from extools.extest import ExTestCase
from extools.exview import exview

VALID_CC_CUSTID = "1400"
"""Customer in SAMINC with valid ISO-3166 code."""

INVALID_CC_CUSTID = "1200"
"""Customer in SAMINC with invalid ISO-3166 code."""

EMPTY_CC_CUSTID = "1210"
"""Customer in SAMINC with empty ISO-3166 code."""

class IsoCountryCodeTestCase(ExTestCase):
    """Unit tests for ISO country code validation."""

    def test_is_valid_iso_cc(self):
        """Passes iff the validation works for a valid, invalid and empty
        country code in SAMINC.

        1. Open a record with a valid country code, verify it validates.
        2. Open a record with an invalid country code, verify validation fails.
        3. Open a record with an empty country code, verify validation fails.

        """

        with exview(AR_CUSTOMER_VIEW, index=self.index,
                    seek_to={'CUSTID': VALID_CC_CUSTID}) as exv:
            self.assert_true(is_valid_iso_cc(exv.get("CODECTRY")))

        with exview(AR_CUSTOMER_VIEW, index=self.index,
                    seek_to={'CUSTID': INVALID_CC_CUSTID}) as exv:
            self.assert_true(not is_valid_iso_cc(exv.get("CODECTRY")))

        with exview(AR_CUSTOMER_VIEW, index=self.index,
                    seek_to={'CUSTID': EMPTY_CC_CUSTID}) as exv:
            self.assert_true(not is_valid_iso_cc(exv.get("CODECTRY")))

class IsoCountryCodeAcceptanceTestCase(ExTestCase):
    """Acceptance tests for ISO country code validation."""

    def test_is_valid_iso_enforced_in_view(self):
        """Verify that a ISO code validation is enforced at the view.

        1. Navigate to a record with an empty country code.
        2. Put an invalid country code in the field (raises ExViewError).
        3. Put a valid country code in the field (succeeds).

        """
        invalid_code = "ZZ"
        valid_code = "CA"

        with exview(AR_CUSTOMER_VIEW, index=self.index,
                    seek_to={'CUSTID':EMPTY_CC_CUSTID}) as exv:

            with self.assert_raises(ExViewError):
                exv.put("CODECTRY", invalid_code)

            exv.put("CODECTRY", valid_code)
